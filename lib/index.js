const {spawn} = require('child_process');


class Exec {
  static start(command, args, needToWait, interval) {
    let doneWithOld = true;
    let numOfExecutions = 0;
    console.log(`Running command '${command}' | Args: ${
        JSON.stringify(
            args)} | interval ${interval}ms | needToWait ${!!needToWait}`);
    setInterval(() => {
      if (needToWait && !doneWithOld) return;
      doneWithOld = false;
      numOfExecutions++;
      const execNum = numOfExecutions;
      console.log(`/******* Staring command #${execNum} ********/`);
      const exec = spawn(command, args);

      exec.stdout.on('data', (data) => {
        console.log(`[${execNum}] ${data}`);
      });

      exec.stderr.on('data', (data) => {
        console.error(`[${execNum}] ${data}`);
      });

      exec.on('close', (code) => {
        console.log(`/******* Ended command #${execNum} ********/`);
        doneWithOld = true;
      });
    }, interval);
  }
}


module.exports = Exec;