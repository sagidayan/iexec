# iexec
**Exe**cute a command with fixed **i**ntervals until you want to stop

run `$ iexec -h` for usage.

```shell
Options:
  -V, --version                  output the version number
  -c, --command <command>        Command to execute wrapped in (default: null)
  -a, --arg [arg]                Arguments for the command (default: [])
  -i, --interval [milliseconds]  Interval milliseconds (default: 1000)
  -w, --wait                     Wait until execution ends before running a new process (default: false)
  -h, --help                     output usage information
```

example:
`$ iexec -c command -a arg1 -a arg2`
will run this command every second:
`$ command arg1 arg2`


## Install
Clone the repo.
run these commands:

```shell
$ npm install #OR yarn install
$ npm link
```

Now you can use the `iexec` command everywhere